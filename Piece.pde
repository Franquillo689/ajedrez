abstract class Piece {
  Position pos;
  boolean isWhite;
  PImage img;
  void drawPiece(int posX, int posY){
    image(img, posX, posY, 100, 100);
  }
  void makeMove(Position finalPos){
    pos.setX(finalPos.x);
    pos.setY(finalPos.y);
    Pawn aux = new Pawn(1,1,true);
    if(this.getClass() == aux.getClass()){
      ((Pawn)this).firstMove = false;
      // Promotion
      if(isWhite && pos.y == 0 || !isWhite && pos.y == 7){
        println("1");
        board.setPiece(new Queen(pos.y, pos.x, !isWhite));
      }
    }
  }
  abstract boolean canMove(Position finalPos);
  abstract void whoami();
}
