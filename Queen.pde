class Queen extends Piece {
  Queen(int posY, int posX, boolean isBlack){
    pos = new Position(posX,posY);
    isWhite = !isBlack;
    if(isWhite){
      img = loadImage("images/whiteQueen.png");
    } else {
      img = loadImage("images/blackQueen.png");
    }
  }
  boolean canMove(Position finalPos){
    return moves.canMoveRect(pos, finalPos) ||
           moves.canMoveDiagonal(pos, finalPos);
  }
  void whoami(){
    println("Queen");
  }
}
