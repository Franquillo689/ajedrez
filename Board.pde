class Board {
  Piece[][] board;
  color[] squaresColor;
  Position lastMousePos;
  boolean isWhiteTurn;
  
  Board(){
    isWhiteTurn = true;
    squaresColor = new color[2];
    squaresColor[0] = #AF5C1C; //Brown
    squaresColor[1] = #F5E4D7; //White
    board = new Piece[8][8];
    // Pawns
    for(int i = 0;i < 8;i++){
      board[1][i] = new Pawn(1,i,true);
      board[6][i] = new Pawn(6,i,false);
    }
    // Rooks
    board[0][0] = new Rook(0,0,true);
    board[0][7] = new Rook(0,7,true);
    board[7][0] = new Rook(7,0,false);
    board[7][7] = new Rook(7,7,false);
    // Knights
    board[0][1] = new Knight(0,1,true);
    board[0][6] = new Knight(0,6,true);
    board[7][1] = new Knight(7,1,false);
    board[7][6] = new Knight(7,6,false);
    // Bishops
    board[0][2] = new Bishop(0,2,true);
    board[0][5] = new Bishop(0,5,true);
    board[7][2] = new Bishop(7,2,false);
    board[7][5] = new Bishop(7,5,false);
    // Queens
    board[0][3] = new Queen(0,3,true);
    board[7][3] = new Queen(7,3,false);
    // Kings
    board[0][4] = new King(0,4,true);
    board[7][4] = new King(7,4,false);
  }
  
  void setPiece(Piece p){
    if(p == null) return;
    board[p.pos.y][p.pos.x] = p;
  }
  
  void movePiece(Position initialPos, Position finalPos){
    getPiece(initialPos).makeMove(finalPos);
    board[finalPos.y][finalPos.x] = board[initialPos.y][initialPos.x];
    board[initialPos.y][initialPos.x] = null;
    getPiece(finalPos).whoami();
    redraw();
  }
  
  void updateMouse(){
    if(lastMousePos == null){
      lastMousePos = getMousePos();
      if(getPiece(lastMousePos) == null || getPiece(lastMousePos).isWhite != isWhiteTurn){
        lastMousePos = null;
      }
    } else {
      Position currentMousePos = getMousePos();
      Piece pieceToMove = getPiece(lastMousePos);
      if(pieceToMove.canMove(currentMousePos)){
        movePiece(lastMousePos,currentMousePos);
        isWhiteTurn = !isWhiteTurn;
      }
      lastMousePos = null;
    }
  }
  
  void drawBoard(){
    noStroke();
    boolean isWhite = true;
    for(int i = 0;i < 8;i++){
      for(int j = 0;j < 8;j++){
        if(isWhite){
          fill(squaresColor[1]);
        } else {
          fill(squaresColor[0]);
        }
        isWhite = !isWhite;
        square(j*(width/8), i*(height/8),width/8);
        if(board[i][j] != null){
          board[i][j].drawPiece(j*(width/8), i*(height/8));
        }
      }
      isWhite = !isWhite;
    }
  }
  
  Piece getPiece(Position pos){
    return board[pos.y][pos.x];
  }
  
  Piece getPiece(int x, int y){
    return board[y][x];
  }
  
  Position getMousePos(){
    int x,y;
    x = mouseX/(width/8);
    y = mouseY/(height/8);
    return new Position(x,y);
  }
  
  boolean isPieceIn(Position pos){
    // Requires pos != null
    return getPiece(pos.x,pos.y) != null;
  }
  
  boolean isPieceIn(int x, int y){
    return getPiece(x,y) != null;
  }
  
  boolean sameTeam(Position a, Position b){
    return a != null && b != null &&
           !(getPiece(a) == null || getPiece(b) == null) &&
           !(getPiece(a).isWhite ^ getPiece(b).isWhite);
  }
}
