class Rook extends Piece {
  Rook(int posY, int posX, boolean isBlack){
    pos = new Position(posX,posY);
    isWhite = !isBlack;
    if(isWhite){
      img = loadImage("images/whiteRook.png");
    } else {
      img = loadImage("images/blackRook.png");
    }
  }
  boolean canMove(Position finalPos){
    return moves.canMoveRect(pos, finalPos);
  }
  void whoami(){
    println("Rook");
  }
}
