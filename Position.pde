class Position {
  int x,y;
  Position(int posX, int posY){
    x = posX;
    y = posY;
  }
  
  void setX(int posX){
    if(0 <= posX && posX < 8){
      x = posX;
    }
  }
  
  void setY(int posY){
    if(0 <= posY && posY < 8){
      y = posY;
    }
  }
  
  boolean equal(Position pos){
    return pos != null && x == pos.x && y == pos.y;
  }
  
  boolean equal(int x, int y){
    return this.x == x && this.y == y;
  }
  
  boolean validPos(Position pos){
    return pos != null && 
           pos.x >= 0 && pos.x < 8 &&
           pos.y >= 0 && pos.y < 8;
  }
  
  float distance(Position p){
    // Requires p != null
    return sqrt((p.x-x)*(p.x-x)+(p.y-y)*(p.y-y));
  }
  
  int nonEuclideanDistance(Position p){
    // Requires p != null
    return abs(x-p.x)+abs(y-p.y);
  }
  
  boolean areOnSameColumn(Position p){
    return p != null && 
           ( x == p.x );
  }
  
  boolean areOnSameRow(Position p){
    return p != null && 
           ( y == p.y );
  }
  
  boolean areOnSameDiagonal(Position p){
    return p != null && abs(x-p.x) == abs(y-p.y);
  }
}
