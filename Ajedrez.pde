
/*
By convention the coords will be as follows:
·- x
|
y
Then, if a position is Position(x,y) => the equivalent
in board[][] will be board[y][x].
*/

Board board;
Movement moves;

void setup(){
  size(800,800);
  board = new Board();
  board.drawBoard();
  moves = new Movement();
  noLoop();
}

void draw(){
  board.drawBoard();
}
void mousePressed(){
  board.updateMouse();
  
}
