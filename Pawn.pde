class Pawn extends Piece {
  boolean firstMove;
  Pawn(int posY, int posX, boolean isBlack){
    firstMove = true;
    pos = new Position(posX,posY);
    isWhite = !isBlack;
    if(isWhite){
      img = loadImage("images/whitePawn.png");
    } else {
      img = loadImage("images/blackPawn.png");
    }
  }
  boolean canMove(Position finalPos){
    if(finalPos == null || board.sameTeam(pos,finalPos)) return false;
    if((isWhite && finalPos.y < pos.y)||(!isWhite && finalPos.y > pos.y)){
      if(pos.areOnSameColumn(finalPos)){
        if(pos.nonEuclideanDistance(finalPos) == 1){
          return !board.isPieceIn(finalPos);
        } else if(pos.nonEuclideanDistance(finalPos) == 2 && firstMove) {
          if(isWhite){
            return !board.isPieceIn(pos.x, pos.y-1) && !board.isPieceIn(finalPos);
          } else {
            return !board.isPieceIn(pos.x, pos.y+1) && !board.isPieceIn(finalPos);
          }
        }
      } else {
        if(isWhite){
          return (finalPos.equal(pos.x+1, pos.y-1) || finalPos.equal(pos.x-1, pos.y-1)) && board.isPieceIn(finalPos);
        } else {
          return (finalPos.equal(pos.x+1, pos.y+1) || finalPos.equal(pos.x-1, pos.y+1)) && board.isPieceIn(finalPos);
        }
      }
    }
    return false;
  }
  void whoami(){
    println("Pawn");
  }
}
