class Movement {
  boolean canMoveRect(Position initialPos, Position finalPos){
    boolean sameColumn, sameRow;
    if(initialPos == null || finalPos == null){
      return false;
    } else {
      sameColumn = initialPos.areOnSameColumn(finalPos);
      sameRow    = initialPos.areOnSameRow(finalPos);
    }
    if((!sameColumn && !sameRow) || board.sameTeam(initialPos, finalPos)){ 
      return false;
    }
     boolean canMove = true;
    int dir[] = {0,0};
    if(sameRow){
      if(finalPos.x-initialPos.x > 0){
        dir[0] = 1;
      } else{
        dir[0] = -1;
      }
    } else {
      if(finalPos.y-initialPos.y > 0){
        dir[1] = 1;
      } else{
        dir[1] = -1;
      }
    }
    
    int[] i = new int[2];
    i[0] = initialPos.x+dir[0];
    i[1] = initialPos.y+dir[1];
    while((i[0] != finalPos.x || i[1] != finalPos.y) && canMove){
      if(board.isPieceIn(i[0],i[1])){
          canMove = false;
      }
      i[0] += dir[0];
      i[1] += dir[1];
    }
    
    return canMove;
  }
  
  boolean canMoveDiagonal(Position initialPos, Position finalPos){
    if(initialPos == null || finalPos == null){
      return false;
    }
    if(board.sameTeam(initialPos, finalPos)){
      return false;
    }
    if(!initialPos.areOnSameDiagonal(finalPos)){
      return false;
    }
    
    int dirX, dirY;
    if(finalPos.x - initialPos.x > 0){
      dirX = 1;
    } else {
      dirX = -1;
    }
    
    if(finalPos.y - initialPos.y > 0){
      dirY = 1;
    } else {
      dirY = -1;
    }
    
    boolean canMove = true;
    int i = initialPos.x+dirX;
    int j = initialPos.y+dirY;
    while(i != finalPos.x && canMove){
      if(board.isPieceIn(i,j)){
        canMove = false;
      }
      i += dirX;
      j += dirY;
    }
    
    return canMove;
  }
}
