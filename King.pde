class King extends Piece {
  King(int posY, int posX,  boolean isBlack){
    pos = new Position(posX,posY);
    isWhite = !isBlack;
    if(isWhite){
      img = loadImage("images/whiteKing.png");
    } else {
      img = loadImage("images/blackKing.png");
    }
  }
  boolean canMove(Position finalPos){
    return finalPos != null &&
           !board.sameTeam(pos, finalPos) &&
           sqrt(2) >= pos.distance(finalPos);
  }
  void whoami(){
    println("King");
  }
}
