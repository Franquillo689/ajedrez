class Bishop extends Piece {
  Bishop(int posY, int posX, boolean isBlack){
    pos = new Position(posX,posY);
    isWhite = !isBlack;
    if(isWhite){
      img = loadImage("images/whiteBishop.png");
    } else {
      img = loadImage("images/blackBishop.png");
    }
  }
  boolean canMove(Position finalPos){
    return moves.canMoveDiagonal(pos, finalPos);
  }
  void whoami(){
    println("Bishop");
  }
}
