class Knight extends Piece {
  Knight(int posY, int posX, boolean isBlack){
    pos = new Position(posX,posY);
    isWhite = !isBlack;
    if(isWhite){
      img = loadImage("images/whiteKnight.png");
    } else {
      img = loadImage("images/blackKnight.png");
    }
  }
  boolean canMove(Position finalPos){
    return finalPos != null &&
           !board.sameTeam(pos, finalPos)  &&
           !pos.areOnSameRow(finalPos)     &&
           !pos.areOnSameColumn(finalPos)  &&
           pos.nonEuclideanDistance(finalPos) == 3;
  }
  void whoami(){
    println("Knight");
  }
}
